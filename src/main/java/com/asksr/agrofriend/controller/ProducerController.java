package com.asksr.agrofriend.controller;

import org.springframework.web.bind.annotation.*;

@RestController
public class ProducerController {
    @PostMapping("/addproduct")
    public void addProduct()
    {
        System.out.println("Addproduct");
    }

    @GetMapping("/getproduct")
    public void getProduct()
    {
        System.out.println("Getproduct");
    }

    @DeleteMapping("/deleteproduct")
    public void deleteProduct()
    {
        System.out.println("Deleteproduct");
    }

    @PutMapping("/updateproduct")
    public void updateProduct()
    {
        System.out.println("Updateproduct");
    }
}

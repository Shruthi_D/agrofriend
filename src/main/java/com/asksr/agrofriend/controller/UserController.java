package com.asksr.agrofriend.controller;

import com.asksr.agrofriend.config.Constants;
import com.asksr.agrofriend.dto.UserDto;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/user")
@RestController
public class UserController {

    @RequestMapping(Constants.GET_USER_BY_ID)
    public UserDto getUserById(@PathVariable Integer userId) {
        //userService.getUserById(userId)
        return null;
    }

    @RequestMapping(Constants.GET_ALL_USERS)
    public List<UserDto> getAllUsers() {
        //userService.getAllUsers()
        List<UserDto> userDtos = new ArrayList<>();
        UserDto userDto1 = new UserDto();
        userDto1.setUserId(1234);
        userDto1.setUserName("Shridhar B");
        userDtos.add(userDto1);

        UserDto userDto2 = new UserDto();
        userDto2.setUserId(1235);
        userDto2.setUserName("Shruthi D");
        userDtos.add(userDto2);

        return userDtos;
    }

    @RequestMapping(value= Constants.SAVE_USER, method= RequestMethod.POST)
    public void saveUser(@RequestBody UserDto userDto) {
        //userService.getAllUsers()

    }
}

package com.asksr.agrofriend.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductDto
{
    private String productName;
    private String productCode;
    private String productDescription;

    public String getProductName() {
        return productName;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }
}

package com.asksr.agrofriend.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
public class Address {
    @Id
    private int id;
    private String houseNumber;
    private String addressLine;
    private String state;
    private int pinCode;
    private int userId;
    private List<PurchaseOrder> purchaseOrders;
    private Date createdTime;
    private Date updatedTime;

}

package com.asksr.agrofriend.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
public class PurchaseOrder {
    @Id
    private  int id;
    private double totalAmount;
    private double discount;
    private int addressId;
    private  int modeId;
    private List<OrderDetail> orderDetails;
    private Date createdTime;
    private Date updatedTime;

}

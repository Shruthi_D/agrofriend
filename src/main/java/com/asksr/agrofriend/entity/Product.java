package com.asksr.agrofriend.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.awt.*;
import java.util.Date;
@Getter
@Setter
@Entity
public class Product {
    @Id
    private int id;
    private String productName;
    private Image productImage;
    private String description;
    private double availableQuantity;
    private double price;
    private int unitTypeId;
    private int userId;
    private Date createdTime;
    private Date updatedTime;

}

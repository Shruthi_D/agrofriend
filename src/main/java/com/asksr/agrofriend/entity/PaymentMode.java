package com.asksr.agrofriend.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
public class PaymentMode {
    @Id
    private int id;
    private String modeName;
    private List<PurchaseOrder> purchaseOrders;
    private Date createdTime;
    private Date updatedTime;
}

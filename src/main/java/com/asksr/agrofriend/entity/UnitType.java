package com.asksr.agrofriend.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.Date;
@Getter
@Setter
@Entity
public class UnitType {
    @Id
    private int id;
    private String unitName;
    private ArrayList<Product> products;
    private Date createdTime;
    private Date updatedTime;
}
